syntax = "proto3";

option java_multiple_files = false;
option java_package = "com.freeje";
option java_outer_classname = "Api";
option objc_class_prefix = "FSG";
option go_package = "gitlab.com/BetaCompany/Freeje/sdk/go/api;freeje_api";
package com.freeje;

//  PushID identificator
message PushId {
  string id = 1; // push id
  string provider = 2; // the provider (firebase)
}

// Any phone number
message PhoneNumber {
  string number = 1;
  string label = 2;
  int64 createdms = 3; // time in epoch millis
}

/* For methods responding by streams streamed=true
 * meaning not to close the stream and keep sending objects in case of their changes
 * oterwise all objects known at the moment will be output and the answer will be completed.
 * Additionally when the stream mode is activated the first element will always will be an empty object,
 * as a start signal of the countdown (useful in handling reconnection cases)
 */
message RequestMode {
  bool streamed = 1;
  string id = 2; // user payload
}

//Empty package
message Empty {
}

//Call status object
message VoipCall {
  string id = 1;
  string dialNumber = 2;
  string sipServer = 3;
  string wssServer = 4;
}


enum PhoneType {
  FIXED_LINE = 0;
  MOBILE = 1;
  TOLL_FREE = 2;
  OTS = 3;
}

enum CallDirection {
  OUTGOING = 0;
  INCOMING = 1;
}

enum CallMedia {
  SIP = 0; // SIP calls
  FREEJE = 1; // native calls with PUSH signalling
  EXTERNAL = 2; // external calls (both inbound and outbound)
  INTERNAL = 3; // internal service calls
}

/* The value of the document type in the lower three bits contains the format of the stored document
000 JPEG
001 UTF8
Others are not being used yet and are reserved
*/
enum DocumentType {
  ID_SCAN = 0; // scan of an identity document JPEG
  ID = 1; // credentials(details) of an identity document UTF8
  PHOTO = 8; // personal photo JPEG
  NAME = 9; // full name UTF8
  ADDRESS_SCAN = 16; // Scan of the document confirming the address
  ADDRESS = 17; // address UTF8
  DELETED_DOC = 1073741824; // document deleted NONE
}

enum NumberFeature {
  VOICE = 0; // Number supports inbound voice calls
  SMS = 1; // Number supports receiving SMS
  SMS_OUT = 3; // Number supports sending SMS
  FAX = 4; // Number supports faxed t38
  VOICE_OUT = 5; // Number supports outbound voice calls
}

message Document {
  string id = 1; // unique identifier
  DocumentType type = 2;
  bytes data = 3; // document content(text or graphics depending on the type)
}

enum DidStatus {
  SETUP = 0; // in the process of activation
  READY = 1; // activated successfully
  ERROR = 2; // error
  DELETED_DID = 1073741824; // deleted
}

/*
Различные параметры для построения отчетов
*/
message ReportRequestOptions {
  string userId = 1; // requested user
  int64 baseDate = 2; // start time mark from which to build the report
  int64 duration = 3; // time interval in millisesonds that the report should capture
}

message FinanceOperation {

  enum Reason {
    CALL = 0; // call charges
    PAYMENT = 1; // Top up through payment system
    CORRECTION = 2; // Manual correction
    NUMBER = 3; // charges for number subscription(maintenance)
    PAYCHECK = 4; // Invoice creation
  }

  enum Type {
    REGULAR = 0; // usual operation
    LOCK_FUNDS = 1; // blocking of funds by preauthorization
    UNLOCK_FUNDS = 2; // unblocking of funds
  }

  string id = 1; // operation identifier
  string userId = 2; // user identifier
  int64 stamp = 3; // operation timestamp
  Reason reason = 4;
  Type type = 5;
  string description = 6; // text description of operation
  bool incomplete = 7; // sign of a pending transaction
  float balance = 8; // the balance of the main account at the beginning of the transaction
  float lockedFunds = 9; // the amount of blocked funds at the start of the transaction
  float amount = 10; // top up/discharge amount
  string authorName = 11; // the name of the user who made the change (empty if the system)
}

message DidPrice {
  // setup fee charges of the number
  float setup_fee = 1;
  // monthly fee charge of the number
  float monthly_fee = 2;
  // the cost of the incoming single billing interval
  float incoming_rate = 3;
  // duration in seconds of a single incoming billing interval
  int32 incoming_interval = 4;
  // the price of the outgoing single interval of charges
  float outgoing_rate = 5;
  // duration in seconds of a single outgoing charging interval
  int32 outgoing_interval = 6;
  // outbound SMS price
  float outgoing_sms = 7;
  // inbound SMS price
  float incoming_sms = 8;
}

message SearchPeriod {
  int64 from = 1;
  int64 duration = 2;
}
message SearchFilter {
  oneof options {
    string value = 1; // filtering value
    SearchPeriod period = 2;
  }
  bool streamed = 3; // if 'true' then keep the streaming of updates in requested method
}

enum ExceptionCode {
  NUMBER_DOES_NOT_EXISTS = 0;
  INVALID_NUMBER = 1;
  AUTH_ERROR = 2;
  PARAMETERS_ERROR = 3;
  NUMBER_ALREADY_ON_DELETION = 4;
  INVALID_NUMBER_ID = 5;
  ACCOUNT_NOT_ACTIVE = 6;
  DOCUMENTS_REQUIRE = 7;
  RELEASE_ERROR = 8;
  NOT_ENOUGH_MONEY = 9;
  BAD_NUMBER = 10;
  LONG_TEXT_MESSAGE = 11;
  SMS_NOT_SUPPORTED = 12;
  SERVICE_ERROR = 13;
}

message ExceptionMeta {
  ExceptionCode code = 1;
}

message AccountAgreement {
  int64 accepted = 1; // the time in epoch milliseconds UTC that the user agrees with the rules of the service, if 0 then the consent has not yet been obtained
  bool is_eu_resident = 2; // EU resident mark
}

// Limiting rule for metric
message MetricRuleMeta {

  enum Type {// event type that should be used in the metric
    REQUEST = 0; // calling request
    CONNECT = 1; // successfull connection
    PRICE = 2; // call price
    TIME = 3; //  call duration in seconds
  }

  string id = 1; // metrics identifier
  int32 ttlSeconds = 2; // lifetime of the metric event in seconds
  double limit = 3; // limit of the metric value that cannot be exceeded
  string name = 4; // name
  string comment = 5; // comment
  Type type = 6; // metric type
}

//Subcription identifier (for polling changes in more than one profile)
message Subscription {
  string id=1; // subscription key
  int64 seq_no=2; // sequental number within subcription(for receiving updates)
}
