syntax = "proto3";
import "common.proto";
import "admin/did.proto";
import "client/forwarding.proto";

package com.freeje.api.admin;

option java_multiple_files = false;
option java_package = "com.freeje.api.admin";
option java_outer_classname = "ProfileType";
option objc_class_prefix = "ProfileType";
option go_package = "gitlab.com/BetaCompany/Freeje/sdk/go/api/admin;freeje_admin";

// Админский сервис управления пользовательским профилем
service Profile {

  /*
   * Проверка прав на доступ администратора и возврат активного keyspace
   */
  rpc Auth (Empty) returns (Keyspace);

  /*
   * Получить список пользовательских профилей (значение заданное в поле id объекта запроса является фильтром для поиска по имени почте ии телефону).
   */
  rpc ListProfiles (SearchFilter) returns (stream FreejeProfile);

  /*
  * Мониторить изменения профиля FreejeProfile.id
  */
  rpc MonitorProfile (FreejeProfile) returns (stream FreejeProfile);

  /*
  * Обновить данные по профилю и возвратить его текущее состояние
  */
  rpc UpdateProfile (FreejeProfile) returns (FreejeProfile);

  rpc ChargeProfile (Charge) returns (AccountBalance);
  rpc UnlockFinanceOperation (FinanceOperation) returns (FinanceOperation);

  /* Получить данные документа  */
  rpc GetDocumentData (DidDocument) returns (DidDocument);

  /* Получить список активных документов в профиле (без данных)  */
  rpc GetProfileDocuments (FreejeProfile) returns (stream DidDocument);

  /*
  * Добавить в аккаунт все телефыоны указанные в phone
  */
  rpc LinkProfilePhones (FreejeProfile) returns (FreejeProfile);

  /*
   * Получить отчет по финансовым операциям
   */
  rpc BuildFinanceReport (ReportRequestOptions) returns (stream FinanceOperation);
}

message FreejeProfile {
  string id = 1; // идентификатор аккаунта
  string email = 2;
  string photo = 3;
  string name = 4;
  int64 createdms = 5; // время создания профиля
  int64 lastusagems = 6; // время последнего обращения профилем к API
  int64 expiredms = 8; // время до которого аккаунт считается действиетельным (0 - лимит не установлен)
  repeated PhoneNumber phone = 9;
  repeated DidNumber did = 10;
  double funds = 11;
  double lockedFunds = 12;
  FreejeProfileMeta meta = 15;
}

message FreejeProfileMeta {
  string tariffId = 1; // действующий тариф профиля
  string tariffName = 2; // имя действующего тарифа
  int32 concurrency = 3; // максимальное количество одновременно задействованных линий (0 - как задано в тарифе)
  AccountAgreement agreement = 4; // данные по согласию пользователя
  bool locked = 5; // признак блокировки аккаунта
  string first_user_agent = 6; // первый user agent клиента с которого обратился пользователь
  com.freeje.api.client.ForwardingConfig forwarding = 9; // настройки переадресации по умолчанию
  repeated com.freeje.api.client.ForwardingConfig did_forwarding = 10; // specific did forwarding settings
}

message Charge {
  string user_id = 1; // идентификатор пользователя чей баланс меняем
  double delta = 2; // значение на которое следует изменить баланс (положительное - пополнение, отрицательное - списание)
  string notes = 3; // комментарий к действию
}

// Баланс счета
message AccountBalance {
  double funds = 1;
  double lockedFunds = 2;
}

message Keyspace {
  string name = 1; // идентификатор используемого keyspace в базе данных
  string version = 2; // версия бэка
}